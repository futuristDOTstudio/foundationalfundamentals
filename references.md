a list of references: 

the high frontier: human colonies in space *by* gererd k. o'neil

space settlements *by* fred scharmen

the power of geography: ten maps that reveal the future of our world *by* tim marshall

isaac newton: the principia, mathematical principle of natural philosophy *by* bernard cohen + anne whitman

code: The Hidden Language of Computer Hardware and Software *by* charles petzold

clojure for domain-specific languages *by* ryan d. kelker

clojure for machine learning *by* akhil wali

distributed systems (edition 3.01) *by* andrew s. tanenbaum +  maarten van steen 

ROS robotics projects - second edition *by* ramkumar gandhinathan + lentin joseph

six easy pieces *by* feynman

reactions - an illustrated exploration of elements, molecules and the change in the universe *by* theodore gray

molecules - the elements and the architecture of everything *by* thoedore gray

the elements - a visual exploration of every known atom in the universe *by* theodore gray

skin in the game *by* nassim nicholas taleb

anti-fragile - things that gain from disorder *by* nassim nicholas taleb

the wealth of nations *by* adam smith

beyond good and evil *by* friedrich nietzsche

risk *by* dan gardner

superforecasting - the art & science of prediction *by* philip tetlock & dan gardner

algorithms to live by *by* brian christian & tom griffiths

against empathy *by* paul bloom

the manipulated man *by* esther vilar

the cyber effect *by* mary aiken

status syndrome *by* michael marmot

how to handle a crowd *by* anika gupta

the cosmic serpent - dna and the origins of knowledge

other minds *by* peter godfrey-smith

digital genesis *by* barnatt

weak links - the universal key to stability of netwroks and complex systems *by* peter csermely

how to measure anything in cybersecurity risk *by* douglas w. hubbard
+ richard seiersen

elemental - how the periodic table can now explain (nearly) everything *by* tim james

the brain book - an illustrated guide to its structure, functions and disorders *by* rita carter

formgiving *by* big bjarke ingel group

social engineering - the science of human hacking *by* christopher hadnagy

health design thinking - creating products and services for better health *by* bon ku, MD + ellen lupton

a crack in creation - the new power to control evolution *by* jennifer doudna + samuel sternberg

zero to one - notes on startups, or how to build the future … *by* peter thiel + blake masters

the sovereign individual mastering the transition to the information age *by* james dale davidson

serious cryptography - a practical introduction to modern encryption *by* jean-philippe aumasson

plant factory - an indoor vertical farming system for efficient quality food production *by* toyoki kozai + genhua niu + michiko tagaki

the genomic kitchen - your guide to understanding and using the food-gene connection for a lifetime of health *by* amanda archibald, RD

genomics & personalised medicine *by* michael snyder

The End of Power: From Boardrooms to Battlefields and Churches to States, Why Being In Charge Isn't What It Used to Be From Boardrooms to Battlefields and Churches to States, Why Being In Charge Isn't What It Used to Be *by* Moises Naim

Learn Computer Forensics: A beginner's guide to searching, analyzing, and securing digital evidence A beginner's guide to searching, analyzing, and securing digital evidence *by* William Oettinger

Scientific Knowledge and Its Social Problems: Written by Jerome R. Ravetz, 1995 Edition, (2nd Revised edition) *by* Jerome R. Ravetz

Infinite Powers: The Story of Calculus - The Language of the Universe The Story of Calculus - The Language of the Universe *by* Steven Strogatz

User Friendly: How the Hidden Rules of Design are Changing the Way We Live, Work & Play How the Hidden Rules of Design are Changing the Way We Live, Work & Play (Books)
*by* Cliff Kuang, Robert Fabricant

The Beginning of Infinity: Explanations That Transform the World Explanations that Transform The World *by* David Deutsch

Seaweeds: Edible, Available, and Sustainable Edible, Available, and Sustainable
*by* Ole Mouritsen, Mariela Johanse, Jonas Drotner Mouritsen 

The Routledge Handbook of Mapping and Cartography *by* Alexander Kent (Editor), Peter Vujakovic (Editor)

Conflict and Consensus: A General Theory of Collective Decisions *by* Serge Moscovici  (Author), Willem Doise (Author)

Pluralism: Against the Demand for Consensus *by* Nicholas Rescher

Modern Operating Systems *by* Andrew S Tanenbaum

The Antitrust Paradox: A Policy at War With Itself *by* Robert H Bork

The Human Work of Art: A Theological Appraisal of Christianity and the Death of the Artist *by* Dzalto Davor 

The Value of Art: Money, Power, Beauty Money, Power, Beauty *by* Michael Findlay

The Inevitable: Understanding the 12 Technological Forces That Will Shape Our Future Understanding the 12 Technological Forces That Will Shape Our Future
*by* Dr Kevin Kelly

Grand Transitions: How the Modern World Was Made How the Modern World Was Made *by* Vaclav Smil

Microservices with Clojure: Develop event-driven, scalable, and reactive microservices with real-time monitoring *by* Anuj Kumar

Security Engineering: A Guide to Building Dependable Distributed Systems, 3rd Edition *by* Ross Anderson

Scarcity: The True Cost of Not Having Enough *by* Sendhil Mullainathan

Dancing with Qubits: How quantum computing works and how it can change the world *by* Robert S. Sutor

The Aquaponic Farmer: A Complete Guide to Building and Operating a Commercial Aquaponic System *by* Adrian Southern

Token Economy: How the Web3 reinvents the Internet *by* Shermin Voshmgir

Learn Computer Forensics: A beginner's guide to searching, analyzing, and securing digital evidence *by* William Oettinger

Scientific Knowledge and Its Social Problems: Written by Jerome R. Ravetz, 1995 Edition, (2nd Revised edition) *by* Jerome R. Ravetz

The Artist in the Machine: The World of AI-Powered Creativity *by* Arthur I. Miller

Life 3,0: Being Human in the Age of Artificial Intelligence *by* Max Tegmark

The Beginning of Infinity: Explanations That Transform the World *by* David Deutsch

User Friendly: How the Hidden Rules of Design are Changing the Way We Live, Work & Play *by* Cliff Kuang

Mathematics: A Complete Introduction: The Easy Way to Learn Maths (Teach Yourself)*by* Hugh Neill

Calculus: A Complete Introduction: The Easy Way to Learn Calculus (Teach Yourself) *by* Hugh Neill

Infinite Powers: The Story of Calculus - The Language of the Universe
*by* Steven Strogatz

History Year by Year: The ultimate visual guide to the events that shaped the world
published *by* DK

The Stack: On Software and Sovereignty (Software Studies) *by* Benjamin H. Bratton

Cultures and Organizations: Software of the Mind, Third Edition: Software of the Mind: Intercultural Cooperation and Its Importance for Survival *by* Geert Hofstede

Electronics in easy steps *by* Bill Mantovani

Soft Robotics: A DIY Introduction to Squishy, Stretchy, and Flexible Robots (Make) *by* Matthew Borgatti

Probability: For the Enthusiastic Beginner *by* David J. Morin

Introduction to Graph Theory *by* Robin J.Wilson

Game Theory: A Simple Introduction *by* K. H. Erickson

Generative Adversarial Networks Projects: Build next-generation generative models using TensorFlow and Keras *by* Kailash Ahirwar

Basic Writings: Martin Heidegger (Routledge Classics) *by*
Heidegger, Martin

The Routledge Handbook of Mapping and Cartography (Routledge Handbooks) *by*
Kent, Alexander

Piracy: The Intellectual Property Wars from Gutenberg to Gates *by*
Johns, Adrian

The Human Use Of Human Beings: Cybernetics And Society *by* Nobert Wiener *by*

Seaweeds: Edible, Available, and Sustainable *by*
Mouritsen, Ole

Mycelium Running: How Mushrooms Can Help Save the World *by*
Stamets, Paul

Growing Gourmet and Medicinal Mushrooms *by*
Stamets, Paul

Booming Bamboo: the (re)discovery of a sustainable material with endless possibilities *by*
Van der Lugt, Pablo

Pluralism: Against the Demand for Consensus *by*
Rescher, Nicholas


Trust Factor: The Science of Creating High-Performance Companies *by*
Paul J. Zak

The Price of Tomorrow: Why Deflation is the Key to an Abundant Future *by*
Booth, Jeff

The Age of the Crowd: A Historical Treatise on Mass Psychology *by*
Moscovici, Serge

Conflict and Consensus: A General Theory of Collective Decisions *by*
Moscovici, Serge

BIAS : Encyclopaedia of Biases and Heuristics *by*
Research Group, Behavioral

The Supply Chain Revolution: Innovative Sourcing and Logistics for a Fiercely Competitive World *by*
Suman Sarkar

The Richest Man In Babylon - Original Edition *by*
Clason, George S

Meditations: The Philosophy Classic (Capstone Classics) *by*
Aurelius, Marcus

Grand Transitions: How the Modern World Was Made *by*
Smil, Vaclav

The Ethical Algorithm: The Science of Socially Aware Algorithm Design *by*
Kearns, Michael

Graph Machine Learning: Take graph data to the next level by applying machine learning techniques and algorithms *by*
Stamile, Claudio

Artificial Intelligence By Example: Acquire advanced AI, machine learning, and deep learning design skills, 2nd Edition *by*
Rothman, Denis

Operational Logistics: The Art and Science of Sustaining Military Operations (Management for Professionals) *by*
Kress, Moshe

Open Source Intelligence Techniques: Resources for Searching and Analyzing Online Information *by*
Bazzell, Michael

The Structure of Scientific Revolutions: 50th Anniversary Edition *by*
Kuhn, Thomas S.

The Human Work of Art: A Theological Appraisal of Christianity and the Death of the Artist *by*
Dzalto Davor

Scaling up Excellence *by*
Rao, Hayagreeva

The Luxury Strategy: Break the Rules of Marketing to Build Luxury Brands *by*
Kapferer, Jean-Noël

Architecting High-Performance Embedded Systems: Design and build high-performance real-time digital systems based on FPGAs and custom circuits *by*
Ledin, Jim

Crafting Interpreters *by*
Nystrom, Robert

-

Figuring Out The Past: The 3,495 Vital Statistics that Explain World History *by*
Turchin, Peter

Griots and Griottes: Masters of Words and Music (African Expressive Cultures) *by*
Thomas A. Hale

Modern Computer Architecture and Organization: Learn x86, ARM, and RISC-V architectures and the design of smartphones, PCs, and cloud servers *by*
Ledin, Jim

Language and Power: A Resource Book for Students (Routledge English Language Introductions) *by*
Simpson, Paul

The Unfolding Of Language: The Evolution of Mankind`s greatest Invention *by*
Deutscher, Guy

The Transistor Handbook *by*
Kaiser, Cletus J.

Computer Time Travel: How to build a microprocessor from transistors How to build a microprocessor from transistors *by*
Walker, JS

Haskell from the Very Beginning *by*
Whitington, John

On Time: A History of Western Timekeeping *by*
Mondschein, Kenneth

-

The Presentation of Self in Everyday Life *by*
Goffman, Erving

On Inhumanity: Dehumanization and How to Resist It *by*
Smith, David Livingstone

The Network Society *by*
van Dijk, Jan A G M

Illustrated Building Pocket Book (Routledge Pocket Books) *by*
Mcdonald, Roxanna

Architect's Pocket Book (Routledge Pocket Books) *by*
Hetreed, Jonathan

Construction Project Manager's Pocket Book (Routledge Pocket Books) *by*
Cartlidge, Duncan

-


The Spatial Web: How web 3.0 will connect humans, machines and AI to transform the world
René, Gabriel

The World Information War: Western Resilience, Campaigning, and Cognitive Effects (Routledge Advances in Defence Studies)
Clack, Timothy

The Law
Bastiat, Frederic

The Mainspring of Human Progress
Weaver, Henry Grady


Conservation by Proxy: Indicator, Umbrella, Keystone, Flagship, and Other Surrogate Species
Tim Caro

Building the New Economy: Data as Capital
Pentland, Alex

Scaffolding (A Basic Safety Perspective)
Nwankwo, Enyinnaya Lewechi

The Boulez-Cage Correspondence
Nattiez, Jean-Jacques

Field Guide to Citizen Science, The: How You Can Contribute to Scientific Research and Make a Difference
Hoffman, Catherine

Working in Public: The Making and Maintenance of Open Source Software
Nadia Eghbal

Privacy is Power: Why and How You Should Take Back Control of Your Data
Véliz, Carissa

The Haskell School of Music: From Signals to Symphonies
Hudak, Paul

Futureproof: 9 Rules for Humans in the Age of Automation
Roose, Kevin

A Thousand Brains: A New Theory of Intelligence
Hawkins, Jeff

Composing Music for Games: The Art, Technology and Business of Video Game Scoring
Thomas, Chance


The Nature of Drugs: History, Pharmacology, and Social Impact
Shulgin, Alexander

The Master and His Emissary: The Divided Brain and the Making of the Western World
Iain Mcgilchrist

Practical Game Design: Learn the art of game design through applicable skills and cutting-edge insights
Kramarzewski, Adam

Building with Earth: Design and Technology of a Sustainable Architecture Fourth and revised edition
Gernot Minke

Citizen Science: How Ordinary People are Changing the Face of Discovery
Caren Cooper




-


Maps of the Mind
Hampden-Turner, C.

Essential Rammed Earth Construction: The Complete Step-by-Step Guide: 9 (Sustainable Building Essentials Series, 9)
Krahn, Tim J.

Icebergs, Zombies, and the Ultra-Thin: Architecture and Capitalism in the 21st Century
Soules, Matthew

Future Imperfect: Technology and Freedom in an Uncertain World
Friedman, David D

The Theory of the Leisure Class (Oxford World's Classics)
Veblen, Thorstein

The Car That Knew Too Much: Can a Machine Be Moral?
Jean-Francois Bonnefon

Right/Wrong: How Technology Transforms Our Ethics
Juan Enriquez

Simulacra and Simulation (The Body in Theory: Histories of Cultural Materialism)
Jean Baudrillard

For a Critique of the Political Economy of the Sign
Jean Baudrillard

IBM and the Holocaust: The Strategic Alliance Between Nazi Germany and America's Most Powerful Corporation-Expanded Edition
Black, Edwin


Essential Rammed Earth Construction: The Complete Step-by-Step Guide: 9 (Sustainable Building Essentials Series, 9)
Krahn, Tim J.

Diplomacy: Theory and Practice
Berridge, G. R.

The Master Switch: The Rise and Fall of Information Empires
Wu, Tim

The Power of Identity: The Information Age - Economy, Society, and Culture: 2 (Information Age Series): 02
Castells, Manuel

Designing the Mind: The Principles of Psychitecture
Mind, Designing the

What Video Games Have to Teach Us About Learning and Literacy: Revised and Updated Edition
Gee, James Paul


The Righteous Mind: Why Good People are Divided by Politics and Religion
Haidt, Jonathan

Reality is Broken: Why Games Make Us Better and How They Can Change the World
McGonigal, Jane

It Is Dangerous to Be Right When the Government Is Wrong: The Case for Personal Freedom
Andrew P. Napolitano

Cypherpunks: Freedom and the Future of the Internet
Assange, Julian

Fascinating Womanhood
Helen Andelin 

Doublespeak: From "Revenue Enhancement" to "Terminal Living, How Government, Business, Advertisers, and Other Use Language to Deceive You (Rebel Reads
Lutz, William













x








_

valve handbook,
holocracy,
netflix culture freedom and responsibility

_
