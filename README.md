
# start

have an abstract/exec summary paragraph here with links to other directories. include the stakepool/tokenomics to help frame incentives etc (stable or ada or token)

figure out images. include the stacked cards

stakepools, d-Apps, series/programs and tokens will bare different names/brand in most cases, so i foresee there being and interesting cacophonous myriad of interweaving shared resources 

gitlab is the archive/collab environ - the stream is for discoverability, it's a quite small area of interest in terms of potential collaborators and delegators, creating a feedback loop would be quite helpful that also engages a broader reach of folks in order to arrive at the select few who would be natural fits

**d-App.Store** - is the base domain that other dapps built to utilise CºiD (Sovereignty Of Self identity) in its ecosystems would be accessed through. they'd have a high trust afforded by the restrained architecture. CºiD will be used in all of the following services and other places most likely in the future


**sentinel.army** - holistic security, starting with basic cyber security stuff gradually growing out eventually to the world of atoms like close protection exoskeleton suits etc basically the sort of security that'll be sort out in an environment where there will low trust/destabilisation. the price of liberty is eternal vigilance + the only rights you have are the ones you can enforce. it's architecture and road to market so to speak is exciting, really like working on this and super intrigued about where it can go


**wealths.health** - next to security, health is the next thing an individual being internationally autonomous are concerned about. obvious low hanging fruit stuff here in the short term - longer term exploring personal interest like bci etc


**manifold.exchange** - the autonomous dex, arbing the internal ecosystem assets with the external among other things like utxo rebalancing, writing off and on-chain, fetching state and other wallet-y things like enable a personal banker through an agent unique to each iD.
that's actually also how the war-chest (and one is certainly needed) for the endeavour manifests, so things like sovereignty of self bonds etc would fall under it. tokenomics will fall under here

**frontiers.space** - there's just a whole load of blue ocean opportunities in this domain. from creating crypto incentive to hold entities accountable in aspects like clean space debris (we're fucked if the issue isn't taken seriously) to use of accountable use of sats in things like surveillance of ecology and for combat recon etc enforcing the costs of externalities to those who create them then attempt to evade them

**caaast.live** - is for content, creating the tribe. content programs culture.

**njenga.estate** is my personal thing, polymathic pursuits, split into art | architecture. i guess it's an extension of this stuff as it looks at how a sov would go about thinking about his estate, in my case patrilineally, how to bequeath crypto assets etc . also looking to use it for construction related stuff of the various services. like atm i've been working really rudimentarily on building some indoor plant farms. so this ties into the health stuff gene-driven biochemical reactions, health from first principles



foundationalfundamentals

[markdown reference](https://www.markdownguide.org/basic-syntax)
